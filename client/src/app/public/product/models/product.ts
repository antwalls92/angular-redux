export class Product {
  id: number;
  name: string;
  //rates: Rate[]
  price: number;
  photo: string;
  description: string;
}
