import { Component, OnInit } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { PublicModule } from './public.module';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css'],
})
export class PublicComponent implements OnInit {

  constructor() {
    //platformBrowserDynamic().bootstrapModule(PublicModule);
  }

  ngOnInit() {
  }

}
