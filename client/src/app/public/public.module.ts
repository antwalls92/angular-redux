import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ProductComponent } from './product/product.component';
import { RouterModule, Routes, UrlSegment } from '@angular/router';
import { CheckoutComponent } from './checkout/checkout.component';
import { ContactdataformComponent } from './checkout/contactdataform/contactdataform.component';


//const beginsWithController = (route) => (url: UrlSegment[]) => {
//  var routes_length = url.length;
//  return routes_length > 0 && url[0].path.startsWith(route) ? ({ consumed: [url[0]] }) : null;
//};

const routes: Routes = [
  { path: 'products', component: ProductComponent, outlet: "public" },
  { path: '', component: HomeComponent, outlet: "public" },
]


@NgModule({
  
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],

  declarations: [HomeComponent, NavbarComponent, ShoppingCartComponent, ProductComponent, PublicComponent, CheckoutComponent, ContactdataformComponent],
  exports: [],
  //bootstrap: [PublicComponent]
})
export class PublicModule { }
