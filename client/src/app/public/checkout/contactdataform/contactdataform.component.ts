import { Component, OnInit, Input } from '@angular/core';
import { ContactData } from '../models/contactdata';

@Component({
  selector: 'app-contactdataform',
  templateUrl: './contactdataform.component.html',
  styleUrls: ['./contactdataform.component.css']
})
export class ContactDataFormComponent implements OnInit {

  public submited: boolean;
  @Input() public model: ContactData;

  constructor(model: ContactData, onsubmit: Function) {
  }

  ngOnInit() {
    this.submited = false;
  }

  onSubmit() {
    this.onSubmit();
    this.submited = true;
  }

}
