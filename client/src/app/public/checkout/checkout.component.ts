import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ShoppingCart } from '../shopping-cart/models/shopping-cart';
import { LoadShoppingCartFromLocalStorage } from '../../core/store/state/shopping-cart/shopping-cart';
import { ContactData } from './models/contactdata';
import { Invoice } from './models/invoice';
import { CreateInvoice } from '../../core/store/state/invoices/invoices';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  private store: Store<ShoppingCart>
  private shoppingCart: ShoppingCart
  private facturationData: ContactData;
  private deliveringData: ContactData;

  constructor(store: Store<ShoppingCart>) {

    this.store = store;
   
    this.store.dispatch(new LoadShoppingCartFromLocalStorage())

    this.store
      .subscribe(val => this.updateShoppingCart(val));
  }

  updateShoppingCart(state) {
    this.shoppingCart = state.shoppingCart;
    this.facturationData = new ContactData();
    this.deliveringData = new ContactData();
  }

  checkout() {

    var invoice = new Invoice();
    invoice.products = this.shoppingCart.products;
    invoice.facturationData = this.facturationData;
    invoice.deliveringData = this.deliveringData;

    this.store.dispatch(new CreateInvoice(invoice));
  }

  ngOnInit() {
  }

}
