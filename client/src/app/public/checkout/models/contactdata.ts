export class ContactData {
  name: string;
  surname: string;
  adress: string;
  city: string;
  country: string;
  postalcode: string;
}
