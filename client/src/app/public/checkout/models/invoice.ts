import { Product } from '../../product/models/product';
import { ContactData } from './contactdata';

export class Invoice {
  id: number;
  amount: number;
  //rates: Rate[]
  products: Product[];
  facturationData: ContactData;
  deliveringData: ContactData;
}


