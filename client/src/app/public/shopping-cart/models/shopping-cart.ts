import { Product } from '../../product/models/product';


export class ShoppingCart {
  products: Product[];

  constructor() { this.products = [] }

  addProduct(product: Product): void {
    this.products.push(product);
  }

  removeProduct(idProduct: number) : void {
    var indexOf = this.products.findIndex(x => x.id == idProduct);
    this.products.splice(indexOf, 1);
  }
}
