import { initialGlobalState } from '../global.state';
import { Action } from '@ngrx/store';
import { Product } from 'src/app/public/product/models/product';
import { ShoppingCart } from '../../../../public/shopping-cart/models/shopping-cart';
import { Invoice } from '../../../../public/checkout/models/invoice';

export function productReducer(state = initialGlobalState, action: InvoiceAction): Invoice[] {
  switch (action.type) {

    case InvoiceActionTypes.PrepareInvoice: {
      return state.invoices;
    }
    case InvoiceActionTypes.CreateInvoice: {
      return state.invoices;
    }
  }
}

export enum InvoiceActionTypes {

  PrepareInvoice = 'PREPARE_INVOICE',
  CreateInvoice = 'CREATE_INVOICE',
}


export class PrepareInvoice implements Action {
  readonly type = InvoiceActionTypes.PrepareInvoice;
  constructor(public payload: ShoppingCart) { }
}
export class CreateInvoice implements Action {
  readonly type = InvoiceActionTypes.CreateInvoice;
  constructor(public payload: Invoice) { }
}



export type InvoiceAction = PrepareInvoice | CreateInvoice ;
