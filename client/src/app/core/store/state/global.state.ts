import { Product } from 'src/app/public/product/models/product';
import { ShoppingCart } from 'src/app/public/shopping-cart/models/shopping-cart';
import { Invoice } from '../../../public/checkout/models/invoice';


export interface GlobalState {
  products: Product[];
  invoices: Invoice[];
  shoppingCart: ShoppingCart;
}

export const initialGlobalState: GlobalState = {
  products: [],
  invoices: [],
  shoppingCart: new ShoppingCart()
}
