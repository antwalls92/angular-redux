import { initialGlobalState } from '../global.state';
import { Action } from '@ngrx/store';
import { Product } from 'src/app/public/product/models/product';

export function productReducer(products = initialGlobalState.products, action: ProductAction): Product[] {
  switch (action.type) {

    case ProductActionTypes.DeleteProduct: {
      var indexOf = products.findIndex(x => x.id == action.payload);
      return products.splice(indexOf, 1);
    }
    case ProductActionTypes.PutProduct: {
      products.push(action.payload)
      return products;
    }
  }
}

export enum ProductActionTypes {

  DeleteProduct = 'DELETE_PRODUCT',
  PutProduct = 'PUT_PRODUCT',
}

export class DeleteProduct implements Action {
  readonly type = ProductActionTypes.DeleteProduct;
  constructor(public payload: number) { }
}

export class PutProduct implements Action {
  readonly type = ProductActionTypes.PutProduct;
  constructor(public payload: Product) { }
}

export type ProductAction = DeleteProduct | PutProduct;
