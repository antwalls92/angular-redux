import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../admin/home/home.component';
import { AdminRoutingModule } from './router/admin-routing.module';



@NgModule({
  declarations: [AdminComponent, HomeComponent],
  imports: [
    CommonModule, RouterModule, AdminRoutingModule
  ],
  //bootstrap: [AdminComponent],
})
export class AdminModule { }
