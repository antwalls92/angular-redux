import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, UrlSegment } from '@angular/router';
import { PublicComponent } from '../public/public.component';
import { ProductComponent } from '../public/product/product.component';
import { HomeComponent } from '../public/home/home.component';
import { CheckoutComponent } from '../public/checkout/checkout.component';


const areas: Routes = [


      { path: 'public', component: HomeComponent },
      { path: 'public/products', component: ProductComponent },
      { path: 'checkout', component: CheckoutComponent },
      { path: 'admin', component: HomeComponent },
      //{ path: '**', component: HomeComponent },
      { path: '', redirectTo: '/public', pathMatch: 'full' },
  
]



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(areas, { enableTracing: true, relativeLinkResolution: "corrected" })
  ],

  exports: [RouterModule]
})

export class RoutingModule { }
