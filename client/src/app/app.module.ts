import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

import { RoutingModule } from './routing/routing.module';
import { HomeComponent } from './public/home/home.component';
import { ProductComponent } from './public/product/product.component';
import { NavbarComponent } from './public/navbar/navbar.component';
import { ShoppingCartComponent } from './public/shopping-cart/shopping-cart.component';
import { CheckoutComponent } from './public/checkout/checkout.component';



@NgModule({

  imports: [
    BrowserModule,
    FormsModule,
    CoreModule,
    RoutingModule,
  ],

  declarations: [
    AppComponent,
    HomeComponent,
    ProductComponent,
    NavbarComponent,
    ShoppingCartComponent,
    CheckoutComponent

  ],

  exports: [],

  bootstrap: [AppComponent]
})

export class AppModule {
  
}
